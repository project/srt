var TS;

function TS_init() {
  TS = {
  'chunks': [], 'splitted' : false, 'blocks': 0,
  'step': 0, 'speed': 120, 'markers': [],
  'paragraph': 0, 'stop': 0};
}

function TS_switch_buttons($type) {
  if ($type == 'start') {
    $('#srt_pause').css('display', 'none');
    $('#srt_stop').css('display', 'none');
    $('#srt_start').css('display', 'inline');
  }
  else if($type == 'stop') {
    $('#srt_start').css('display', 'none');
    $('#srt_pause').css('display', 'inline');
    $('#srt_stop').css('display', 'inline');
  }
}

function TS_next() {
  if (TS.step < TS.chunks.length) {
    // Check if user wants to stop streamming
    if (TS.stop == 1) {
      TS.stop = 0;
      return;
    }
    else if(TS.stop == 2){
      TS_init();
      return;
    }
    
    // Display paragraph and format page
    var _p, _page = '', _pageed = false, _w = 0, _paragraph = '';
    for (_p=0; _p < TS.blocks.length; ++_p) {
      if (TS.step < TS.markers[_p] && !_pageed) {
        // Highlight paragraph
        _page += '<span class="current">' + TS.blocks[_p] + '</span>';
        // Display paragraph
        for (_w=TS.markers[_p -1]; _w < TS.markers[_p]; ++_w) {
          if (_w == TS.step)
            _paragraph += '<span class="current">' + TS.chunks[_w] + '</span> ';
          else
            _paragraph += TS.chunks[_w] + ' ';
        }
        $('#srt_paragraph').html(_paragraph);
        _pageed = true;
      }
      else
        _page += TS.blocks[_p];
    }
    
    // Display page
    $('#srt_page').html(_page.replace(/\x0A/g,'<br />'));
    var pScroll = $('#srt_page span').offset().top - $('#srt_page').offset().top;
    $('#srt_page').animate({scrollTop: '+=' + pScroll + 'px'}, 12);

    // Set next paragraph
    if (TS.step >= TS.markers[TS.paragraph])
      ++TS.paragraph;

    // Get next word
    var _word = TS.chunks[TS.step];
    // Count of characters
    _chars = _word.length;
    // Display word
    $('#srt_stream div').html(_word);
    // Calculate reading speed
    _coef = (1000*60)/TS.speed;
    
    // Let's display next word!
    setTimeout('TS_next()', _coef);
    ++TS.step;
  }
  else {
    // Reset counter
    TS_init();
    TS_switch_buttons('start');
  }
}

$(document).ready(function () {
  TS_init();
  $('#srt_start').click(function() {
    // Get Stream
    var _stream = $('#srt_text textarea').val();
    // Pre-format paragraph separators
    _separators = {0: '.', 1: ';', 2: '\x0A', 'length': 3};
    for (_s = 0; _s < _separators.length; ++_s)
      _stream = _stream.replace(
        new RegExp('['+_separators[_s]+']', 'g'),
        _separators[_s]+'<!--sep-->'
      );
    // Get blocks
    TS.blocks = _stream.split('<!--sep-->');
    // Get chunks
    var _chunks = 0, _w = 0, _p = 0;
    for (_p=0; _p < TS.blocks.length; ++_p) {
      _words = TS.blocks[_p].split(/[\s]/g);
      for (_w=0; _w<_words.length; ++_w)
        TS.chunks[_chunks+_w] = _words[_w];
      _chunks += _words.length;
      TS.markers[_p] = _chunks;
    }
    TS.markers[-1] = 0;
    // Get reading speed
    TS.speed = $('#srt_speed input').val();
    TS.splitted = true;
    // Start streamming
    TS_next();
    TS_switch_buttons('stop');
  });
  $('#srt_pause').click(function() {
    TS.stop = 1;
    TS_switch_buttons('start');
  });
  $('#srt_stop').click(function() {
    TS.stop = 2;
    TS_switch_buttons('start');
  });
});
